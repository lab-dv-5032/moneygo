//user actions
export const saveUser = (payload) => ({
    type: 'LOGIN_SUCCESS',
    payload
})

//wallet actions
export const addWallet = (payload) => ({
    type: 'ADD_WALLET',
    payload
})

export const updateWallet = (payload) => ({
    type: 'UPDATE_WALLET',
    payload 
})

export const getWallet = (payload) => ({
    type: 'GET_WALLET',
    payload
})
