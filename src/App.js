import React, {Component} from 'react';
import Router from './system/Router'
import { Provider } from 'react-redux'
import  store  from './system/Store'


export default class App extends Component{
  render(){
    return (
      <Provider store={store}>
        <Router/>
      </Provider>
    )
  }
}
