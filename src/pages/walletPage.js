import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { WhiteSpace, Button } from '@ant-design/react-native'
import { getWallet } from '../actions/actions'

class Transactions extends Component {

    state = {
        name: this.props.name,
        balance: this.props.balance,
        transactions: this.props.transactions
    }

    UNSAFE_componentWillMount() {
        console.log('transaction wallet', this.state)
    }

    _renderListItem = (item) => {
        return (<FlatList
            data={item}
            extraData={this.state}
            keyExtractor={(item) => item.key}
            renderItem={({ item }) => (
                <View></View>
            )}
        />)
    }

    onPressAddTransaction = () => {
        this.props.history.push('/addTransaction')
    }

    onPressHome = () => {
        this.props.history.push('/main')
    }

    onPressProfile = () => {
        this.props.history.push('/profile')
    }


    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.11, flexDirection: 'row', width: '100%', backgroundColor: 'grey' }}>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressHome}>
                            <Text>Home</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center', marginHorizontal: 20 }}>
                        <Text>moneyGo</Text>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressProfile}>
                            <Text>profile</Text>
                        </TouchableOpacity>
                    </View>


                </View>

                <View style={{ height: '20%', backgroundColor: 'grey' }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{flex: 1, flexDirection:'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 16}}>{this.state.name}</Text>
                            <Text style={{fontSize: 20}}>{this.state.balance}</Text>
                        </View>
                        <TouchableOpacity onPress={this.onPressAddTransaction}>
                            <Text style={{ fontSize: 16 }}>add transaction</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <FlatList
                    data={this.state.transactions}
                    extraData={this.state}
                    style={{ width: '100%' }}
                    keyExtractor={(item) => item.key}
                    renderItem={({ item }) => (
                        <View>
                            <Text>{item.date.toDateString()}</Text>
                        </View>
                    )}
                />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.wallet.name,
        balance: state.wallet.balance,
        transactions: state.wallet.transactions
    }
}

export default connect(mapStateToProps, { getWallet })(Transactions)