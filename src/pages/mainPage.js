import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { getWallet } from '../actions/actions'
import { WhiteSpace, Button } from '@ant-design/react-native'




class MainPage extends Component {

    state = {
        email: this.props.email,
        password: this.props.password,
        firstName: this.props.firstName,
        lastName: this.props.lastName,
        token: this.props.token,
        wallets: this.props.wallets
    }


    UNSAFE_componentWillMount() {
        console.log('state main page', this.state)
        // axios.get("https://zenon.onthewifi.com/moneyGo/users")
        //     .then((response) => console.log('response mainPage', response.data))
        //     .catch((e) => console.log('error mainPage', e.response))

    }

    onPressWallet = (item) => {
        this.props.getWallet(item)
        this.props.history.push('/wallet')
    }

    onPressAddWallet = () => {
        this.props.history.push('/addWallet')
    }

    onPressHome = () => {
        this.props.history.push('/main')
    }

    onPressProfile = () => {
        this.props.history.push('/profile')
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.11, flexDirection: 'row', width: '100%', backgroundColor: 'grey' }}>

                    <View style={{ flex:1,justifyContent: 'center', alignItems:'flex-start', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressHome}>
                            <Text>Home</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex:2,justifyContent: 'center', alignItems:'center', marginHorizontal: 20 }}>
                        <Text>moneyGo</Text>
                    </View>

                    <View style={{ flex:1,justifyContent: 'center', alignItems:'flex-end', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressProfile}>
                            <Text>profile</Text>
                        </TouchableOpacity>
                    </View>
                    

                </View>

                <View style={{ flex: 1, flexDirection: 'column', margin: 30 }}>
                    <Text style={{ fontSize: 24, alignItems: 'center' }}>Wallets</Text>
                    <FlatList
                        data={this.state.wallets}
                        extraData={this.state}
                        style={{ width: '100%' }}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) => (
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.onPressWallet(item)}>
                                    <Text style={{ fontSize: 16, justifyContent: 'flex-start', alignItems: 'center' }}>{item.name}</Text>
                                    <WhiteSpace size='sm' />
                                    <Text style={{ fontSize: 20, justifyContent: 'flex-end', alignItems: 'center' }}>{item.balance}</Text>
                                    <WhiteSpace size='xl' />
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    <Button type="primary" onPress={this.onPressAddWallet}>Add Wallet</Button>
                </View>

            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        email: state.login.email,
        password: state.login.password,
        firstName: state.login.firstName,
        lastName: state.login.lastName,
        token: state.login.token,
        wallets: state.wallet
    }
}
export default connect(mapStateToProps, { getWallet })(MainPage)