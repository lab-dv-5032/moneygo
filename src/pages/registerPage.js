import React, { Component } from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'
import axios from 'axios'
import { InputItem, WhiteSpace, Button } from '@ant-design/react-native'


export default class RegisterPage extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    }

    onSubmit = () => {
        console.log('onSubmit called')
        if (!this.state.firstName) {
            alert('กรุณากรอกชื่อจริง')
        } else if (!this.state.lastName) {
            alert('กรุณากรอกนามสกุล')
        } else if (!this.state.email) {
            alert('กรุณากรอกอีเมล')
        } else if (!this.state.password) {
            alert('กรุณากรอกรหัสผ่าน')
        } else if (!this.state.confirmPassword) {
            alert('กรุณายืนยันรหัสผ่าน')
        } else if (this.state.password !== this.state.confirmPassword) {
            alert('กรุณายืนยันรหัสผ่านใหม่อีกครั้ง')
        } else {
            axios.post('https://zenon.onthewifi.com/moneyGo/users/register',
                {
                    email: this.state.email,
                    password: this.state.password,
                    firstName: this.state.firstName,
                    lastName: this.state.lastName
                })
                .then((response) => { console.log(response) })
                .catch((error)=>{
                    console.log(error)
                    alert('อีเมลชื่อนี้มีผู้ใช้แล้ว กรุณาใช้อีเมลชื่ออื่น')
                })
        }
    }

    backToLogin = () => {
        this.props.history.push('/login')
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                    <View style={{ flex: 2, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '80%' }}>
                        <WhiteSpace size="xl" />
                        <Text style={{ fontSize: 24 }}>Register</Text>
                        <WhiteSpace size="xl" />
                        <InputItem placeholder='First name' onChangeText={(text) => { this.setState({ firstName: text }) }} />
                        <WhiteSpace size="xl" />
                        <InputItem placeholder='Last name' onChangeText={(text) => { this.setState({ lastName: text }) }} />
                        <WhiteSpace size="xl" />
                        <InputItem placeholder='email' onChangeText={(text) => { this.setState({ email: text }) }} />
                        <WhiteSpace size="xl" />
                        <InputItem placeholder='password' onChangeText={(text) => { this.setState({ password: text }) }} />
                        <WhiteSpace size="xl" />
                        <InputItem placeholder='confirm password' onChangeText={(text) => { this.setState({ confirmPassword: text }) }} />
                    </View>

                    <WhiteSpace size="xl" />
                    <WhiteSpace size="xl" />
                    <WhiteSpace size="xl" />

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Button type="primary" onPress={this.onSubmit} style={{marginHorizontal: 20}}>Submit</Button>
                        <Button type="primary" onPress={this.backToLogin} style={{marginHorizontal: 20}}>Back</Button>
                    </View>
                </View>
            </ScrollView>
        )
    }
}
