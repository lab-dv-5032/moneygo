import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { InputItem, Button, WhiteSpace } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { addWallet } from '../actions/actions'

class AddWallet extends Component {

    state = {
        id: '',
        name: '',
        balance: 0.00,
        transactions: []
    }

    UNSAFE_componentWillMount() {
        this.setState({ wallets: this.props.wallets })
        console.log('wallet list', this.props.wallets)
    }

    onSubmit = () => {
        if (!this.state.name && !this.state.balance) {
            alert("กรุณากรอกชื่อกระเป๋าเงิน และจำนวนเงินในกระเป๋าเงิน")
        } else if (!this.state.name) {
            alert("กรุณากรอกชื่อกระเป๋าเงิน")
        } else if (!this.state.balance) {
            alert("กรุณากรอกจำนวนเงินในกระเป๋าเงิน")
        } else {
                console.log('added wallet', this.state)
                this.props.addWallet(this.state)
                this.props.history.push('/main')
            
        }
    }

    onCancle = () => {
        this.props.history.push('/main')
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ alignItems: 'center', margin: 30 }}>
                    <Text style={{ fontSize: 24 }}>Add Wallet</Text>
                    <WhiteSpace size="xl" />
                    <View style={{ width: '90%' }}>
                        <InputItem placeholder="wallet's title" onChangeText={(text) => this.setState({ name: text })} />
                        <InputItem type="number-pad" placeholder="amount of money" onChangeText={(text) => this.setState({ balance: text })} />
                    </View>
                    <WhiteSpace size="xl" />
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Button type="primary" onPress={this.onSubmit} style={{ marginHorizontal: 20 }}>Add wallet</Button>
                        <Button type="primary" onPress={this.onCancle} style={{ marginHorizontal: 20 }}>Cancle</Button>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        wallets: state.wallet
    }
}

export default connect(mapStateToProps, { addWallet })(AddWallet)