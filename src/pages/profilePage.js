import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { WhiteSpace, Button } from '@ant-design/react-native'

class ProfilePage extends Component {

    state = {
        email: this.props.email,
        password: this.props.password,
        firstName: this.props.firstName,
        lastName: this.props.lastName,
        token: this.props.token,
        wallets: this.props.wallets
    }

    onPressHome = () => {
        this.props.history.push('/main')
    }

    onPressProfile = () => {
        this.props.history.push('/profile')
    }
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.11, flexDirection: 'row', width: '100%', backgroundColor: 'grey' }}>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressHome}>
                            <Text>Home</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center', marginHorizontal: 20 }}>
                        <Text>moneyGo</Text>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressProfile}>
                            <Text>profile</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 20}}>Profile Page</Text>
                        <Text style={{fontSize: 16}}>First name: {this.state.firstName}</Text>
                </View>

            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        email: state.login.email,
        password: state.login.password,
        firstName: state.login.firstName,
        lastName: state.login.lastName,
        token: state.login.token,
        wallets: state.wallet
    }
}
export default connect(mapStateToProps)(ProfilePage)