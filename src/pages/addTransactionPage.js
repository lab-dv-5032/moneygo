import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { InputItem, Button, WhiteSpace, DatePicker, List } from '@ant-design/react-native'
import { getWallet, updateWallet } from '../actions/actions'


class AddTransaction extends Component {

    onPressCancle = () => {
        this.props.history.push('/wallet')
    }

    onPressHome = () => {
        this.props.history.push('/main')
    }

    onPressProfile = () => {
        this.props.history.push('/profile')
    }
    
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>

                <View style={{ flex: 0.11, flexDirection: 'row', width: '100%', backgroundColor: 'grey' }}>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressHome}>
                            <Text>Home</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center', marginHorizontal: 20 }}>
                        <Text>moneyGo</Text>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginHorizontal: 20 }}>
                        <TouchableOpacity onPress={this.onPressProfile}>
                            <Text>profile</Text>
                        </TouchableOpacity>
                    </View>


                </View>

                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', width: '80%' }}>
                    <Text>Add Transaction</Text>
                    <InputItem placeholder="title" />
                    <InputItem type="number-pad" placeholder="price" />
                    <InputItem placeholder="description" />
                    {/* <DatePicker>
                    <List.Item arrow="horizontal">Date</List.Item>
                </DatePicker> */}
                    <Button type="primary">Save</Button>
                    <Button type="primary" onPress={this.onPressCancle}>Cancle</Button>
                </View>

            </View>
        )
    }
}

export default connect(null, { getWallet, updateWallet })(AddTransaction)