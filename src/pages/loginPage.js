import React, { Component } from 'react'
import { View, Text, TextInput, Image, ScrollView } from 'react-native'
import axios from 'axios'
import { connect } from 'react-redux'
import { InputItem, WhiteSpace, Button } from '@ant-design/react-native'
import { saveUser } from '../actions/actions'


class LoginPage extends Component {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        token: '',
        isLoggedIn: false
    }

    onPressRegister = () => {
        this.props.history.push('/register')
    }

    onPressLogin = () => {

        if (!this.state.username) {
            alert('Please enter email')
        } else if (!this.state.password) {
            alert('Please enter password')
        } else if (!this.state.username && !this.state.password) {
            alert('Please enter email and password')
        } else {
            axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.username,
                password: this.state.password
            })
                .then((response) => {
                    // console.log('call loginSuccess')
                    console.log(response.data.user)
                    this.setState({
                        email: response.data.user.email,
                        password: response.data.user.password,
                        firstName: response.data.user.firstName,
                        lastName: response.data.user.lastName,
                        token: response.data.user.token,
                        isLoggedIn: true
                    })
                    console.log('state login', this.state)
                    this.props.saveUser(this.state)
                    this.props.history.push('/main')
                })
                .catch((e) => { console.log('error loginPage', e) })
        }

    }

    render() {

        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1, padding: 20, flexDirection: 'column' }}>
                    <View style={{ flex: 1, margin: 10 }}>
                        <Text>Welcome to</Text>
                        <Image style={{ width: '100%', height: 150 }} source={{ uri: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.15752-9/52983118_395034664379584_2589569519450062848_n.png?_nc_cat=109&_nc_ht=scontent-kut2-1.xx&oh=bd5d381d4dabab8155d228f0d178378f&oe=5D18FE6C' }} />

                    </View>
                    <WhiteSpace size="xl" />
                    <View style={{ flex: 2 }}>
                        <Text>Login</Text>
                        <InputItem placeholder='Username' onChangeText={(text) => { this.setState({ username: text }) }} />
                        <WhiteSpace size="xl" />
                        <InputItem type="password" placeholder='Password' onChangeText={(text) => { this.setState({ password: text }) }} />
                    </View>
                    <WhiteSpace size="xl" />
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Button type='primary' onPress={this.onPressLogin} style={{ width: '60%' }}>Login</Button>
                        <WhiteSpace size="xl" />
                        <Button type='primary' onPress={this.onPressRegister} style={{ width: '60%' }}>Register</Button>
                    </View>
                </View>
            </ScrollView>

        )
    }

}

export default connect(null, { saveUser })(LoginPage)
