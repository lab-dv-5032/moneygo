export default (state = DEFAULT_WALLETES, action) => {
    
    switch(action.type) {

        case 'ADD_WALLET': 
        console.log('addWallet reducer payload', action.payload)
        console.log('state wallet reducer', state)
        return [...state, action.payload]

        case 'EDIT_WALLET':
        console.log('editWallet payload', action.payload)
        const newWallet = state.map( ele => {
            return ele._id === action.payload._id ? action.payload : ele
        })
        console.log('state after edit', state)
        return newWallet

        case 'GET_WALLET':
        console.log('getWallet reducer', action.payload)
        return action.payload


        default: return state
    }
}

const DEFAULT_WALLETES = [
    {
        id: '1',
        name: 'myWallet',
        balance: 500.00,
        transactions: [
            {
                date: new Date(2019,1,1),
                list: [
                    {
                        type: 'income',
                        title: 'lorem',
                        catagories: 'work',
                        price: 200.00

                    },
                    {
                        type: 'income',
                        title: 'lorem2',
                        catagories: 'work',
                        price: 300.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem3',
                        catagories: 'food',
                        price: 100.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem4',
                        catagories: 'food',
                        price: 200.00
                    }
                ]
            },
            {
                date: new Date(2019,1,20),
                list: [
                    {
                        type: 'income',
                        title: 'lorem',
                        catagories: 'work',
                        price: 200.00

                    },
                    {
                        type: 'income',
                        title: 'lorem2',
                        catagories: 'work',
                        price: 300.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem3',
                        catagories: 'food',
                        price: 100.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem4',
                        catagories: 'food',
                        price: 200.00
                    }
                ]
            }
        ]
    },{
        id: '2',
        name: 'myWallet2',
        balance: 2500.00,
        transactions: [
            {
                date: new Date(2019,0,19),
                list: [
                    {
                        type: 'income',
                        title: 'lorem',
                        catagories: 'work',
                        price: 200.00

                    },
                    {
                        type: 'income',
                        title: 'lorem2',
                        catagories: 'work',
                        price: 300.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem3',
                        catagories: 'food',
                        price: 100.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem4',
                        catagories: 'food',
                        price: 200.00
                    }
                ]
            },
            {
                date: new Date(2019,1,20),
                list: [
                    {
                        type: 'income',
                        title: 'lorem',
                        catagories: 'work',
                        price: 200.00

                    },
                    {
                        type: 'income',
                        title: 'lorem2',
                        catagories: 'work',
                        price: 300.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem3',
                        catagories: 'food',
                        price: 100.00
                    },
                    {
                        type: 'expense',
                        title: 'lorem4',
                        catagories: 'food',
                        price: 200.00
                    }
                ]
            }
        ]

    }
]