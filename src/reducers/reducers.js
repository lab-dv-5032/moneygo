import { combineReducers } from 'redux'
import LoginReducers from './LoginReducers'
import WalletReducers from './WalletReducers'

const reducers = combineReducers({
    login: LoginReducers,
    wallet: WalletReducers
})

export default reducers; 