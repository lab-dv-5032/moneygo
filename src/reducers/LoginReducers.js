export default (state = {
    email: 'email',
    password: 'password',
    firstName: 'firstName',
    lastName: 'lastName',
    token: 'token'}, action) => {
    switch (action.type){
        case 'LOGIN_SUCCESS':
        console.log('state reducer',action.payload)
        return { 
            email: action.payload.email,
            password: action.payload.password,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
            token: action.payload.token 
        }       
        
        default: return state
    }

}