import React, { Component } from 'react'
import { Route, Switch, NativeRouter, Redirect } from 'react-router-native'
import LoginPage from '../pages/loginPage'
import MainPage from '../pages/mainPage';
import RegisterPage from '../pages/registerPage';
import AddWallet from '../pages/addWalletPage'
import AddTransaction from '../pages/addTransactionPage'
import WalletPage from '../pages/walletPage'
import ProfilePage from '../pages/profilePage'

export default class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/login" component={LoginPage} />
                    <Route exact path="/main" component={MainPage} />
                    <Route exact path="/register" component={RegisterPage} />
                    <Route exact path="/addWallet" component={AddWallet}/>
                    <Route exact path="/addTransaction" component={AddTransaction}/>
                    <Route exact path="/wallet" component={WalletPage}/>
                    <Route exact path="/profile" component={ProfilePage}/>
                    <Redirect to="/login" />
                </Switch>
            </NativeRouter>
        )
    }
}